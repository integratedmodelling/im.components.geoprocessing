namespace components.geoprocessing.morphology;

number geomorphon_code
	"The result code of the Geomorphon algorithms, classifying land forms according to their shape."
	// leave m for the colormap
	observing geography:Elevation in m
	using im.geomorphology.classifier(radius ?= 0, threshold ?= 0);

@colormap(values = {
	type of earth:Flat: (127 127 127), 
	type of earth:Peak: (108 0 0),
	type of earth:Ridge: (255 0 0),
	type of earth:Shoulder: (255 165 0),
	type of earth:Spur: (255 219 61),
	type of earth:Slope: (255 255 0),
	type of earth:Hollow: (143 203 44),
	type of earth:Footslope: (50 189 160),
	type of earth:Valley: (0 0 255),
	type of earth:Pit: (0 0 0)
})
@documented(geoprocessing.morphology.geomorphon)  
model type of earth:GeoFormation
	observing geography:Elevation in m
	using im.geomorphology.classifier(radius ?= 0, threshold ?= 0)
	classified into 
		type of earth:Flat if 1000,
		type of earth:Peak if 1001,
		type of earth:Ridge if 1002,
		type of earth:Shoulder if 1003,
		type of earth:Spur if 1004,
		type of earth:Slope if 1005,
		type of earth:Hollow if 1006,
		type of earth:Footslope if 1007,
		type of earth:Valley if 1008,
		type of earth:Pit if 1009;
		
@documented(geoprocessing.morphology.geomorphon)
model each earth:Flat 
	observing geomorphon_code
	using gis.features.extract(select = [geomorphon_code == 1000]);
	
@documented(geoprocessing.morphology.geomorphon)
model each earth:Peak
	observing geomorphon_code
	using gis.features.extract(select = [geomorphon_code == 1001]);

@documented(geoprocessing.morphology.geomorphon)
model each earth:Ridge 
	observing geomorphon_code
	using gis.features.extract(select = [geomorphon_code == 1002]);

@documented(geoprocessing.morphology.geomorphon)
model each earth:Shoulder 
	observing geomorphon_code
	using gis.features.extract(select = [geomorphon_code == 1003]);

@documented(geoprocessing.morphology.geomorphon)
model each earth:Spur 
	observing geomorphon_code
	using gis.features.extract(select = [geomorphon_code == 1004]);
	
@documented(geoprocessing.morphology.geomorphon)
model each earth:Slope 
	observing geomorphon_code
	using gis.features.extract(select = [geomorphon_code == 1005]);
		
@documented(geoprocessing.morphology.geomorphon)
model each earth:Footslope 
	observing geomorphon_code
	using gis.features.extract(select = [geomorphon_code == 1007]);

@documented(geoprocessing.morphology.geomorphon)
model each earth:Valley 
	observing geomorphon_code
	using gis.features.extract(select = [geomorphon_code == 1008]);

@documented(geoprocessing.morphology.geomorphon) 
model each earth:Pit 
	observing geomorphon_code
	using gis.features.extract(select = [geomorphon_code == 1009]);
	