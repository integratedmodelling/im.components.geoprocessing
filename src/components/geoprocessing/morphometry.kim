namespace components.geoprocessing.morphometry;

@documented(hydrology.morphometry.pitfilledelevation)
model hydrology:HydrologicallyCorrected geography:Elevation in m
	observing geography:Elevation in m
	using im.hydrology.fillsinks();
	
number flow_directions_d8_hydrological
	"The conventional D8 flow direction code, equivalent to 2**n where n is counting 
	 clockwise with W = 0. Computed using the more expensive flow direction method that
     requires a hydrologically corrected DEM."
	observing hydrology:HydrologicallyCorrected geography:Elevation
	using im.hydrology.flowdirections(angles = false);
	
@documented(hydrology.morphometry.flowdirectioncode)
number flow_directions_d8
	"The conventional D8 flow direction code, equivalent to 2**n where n is counting 
	 clockwise with W = 0."
	observing geography:Elevation
	using im.hydrology.leastcostflowdirections();

number upstream_cell_count
	observing geography:Elevation
	using im.hydrology.leastcostflowdirections(dotca=true);	
	
@documented(hydrology.morphometry.flowdirections)
model hydrology:FlowDirection in degree_angle
	observing geography:Elevation in m
	using im.hydrology.leastcostflowdirections(angles=true);

@documented(hydrology.morphometry.contributingarea)
// FIXME shouldn't need extensive because the concept is "of" watershed. Probably only relaxing
// 
@extensive(space)
model hydrology:ContributingArea within hydrology:HydrologicallyCorrected hydrology:Watershed in m^2
	observing geography:Elevation
	using im.hydrology.leastcostflowdirections(dotca=true, areas=true);

//number upstream_cell_count
//	"The number of areal subdivisions upstream of each extent. Guaranteed correct
//	 only in a raster context."
//	observing flow_directions_d8
//	using im.hydrology.flowaccumulation(cells = true);
	
model presence of earth:Stream 
	observing upstream_cell_count
	using im.hydrology.streamnetwork(tca.threshold ?= 0.001);
	
model each hydrology:StreamOutlet
	observing flow_directions_d8, upstream_cell_count
	using im.hydrology.extractoutlets();


	